class App extends Base {
    constructor() {
        super();
        this.questions;
        this.resultConditions;
        this.sumSources;
        this.user = new User(this);
        this.loadFromJSON();
        this.eventHandlers();
        this.renderStart();
    }

    loadFromJSON() {
        let that = this;
        $.getJSON('/json/questions.json', function (data) {
            that.questions = data.questions;
            that.resultConditions = data.resultConditions;
            that.sumSources = data.sumSources;
        });
    }

    evaluateScore() {
        //set a score for the results, sum of value of answers
        for (let i = 0; i < this.user.answers.length; i++) {
            for (let x = 0; x < Object.keys(this.sumSources).length; x++) {
                for (let y = 0; y < this.sumSources[Object.keys(this.sumSources)[x]].questionIds.length; y++) {
                    if ((i + 1) === this.sumSources[Object.keys(this.sumSources)[x]].questionIds[y]) {
                        this.user.score[0][Object.keys(this.sumSources)[x]] = parseInt(this.user.answers[i].val) + this.user.score[0][Object.keys(this.sumSources)[x]];
                    }
                }
            };
        }
        //set a string from the questions.json
        let count = 0;
        for (let i = 0; i < Object.keys(this.resultConditions).length; i++) {
            let evalString = "";
            if (Object.keys(this.resultConditions)[i].split("&&").length == 1) {
                evalString = `this.user.score[0].${Object.keys(this.resultConditions)[i].split(" && ")[0]}`;
            } else {
                evalString = `this.user.score[0].${Object.keys(this.resultConditions)[i].split(" && ")[0]} && this.user.score[0].${Object.keys(this.resultConditions)[i].split(" && ")[1]}`;
            }

            if (eval(evalString)) {
                this.user.results[0][Object.keys(this.user.results[0])[count]] = Object.values(this.resultConditions)[i];
                count++;
            }
        }
        this.user.saveAnswers();
    }

    eventHandlers() {
        let that = this;
        super.registerEventHandlers({
            btnNext: function () {
                if ($(this).hasClass("btnNext")) {
                    console.log(this.id)
                    that.user.answer((parseInt(this.id) - 1), $("#slider")[0].value);
                    if (this.id <= 23) {
                        that.renderQuestion(parseInt(this.id));
                    } else {
                        that.renderScore();
                    }
                }
            },
            btnPrevious: function () {
                if ($(this).hasClass("btnPrevious")) {
                    if (this.id >= 0) {
                        that.user.answer((parseInt(this.id) + 1), $("#slider")[0].value);
                        that.renderQuestion(parseInt(this.id));
                    }
                }
            },
            btnStart: function () {
                if ($(this).hasClass("btnStart")) {
                    that.renderQuestion(0);
                }
            }
        });
    }

    renderStart() {
        super.render(`
        <section class="row mt-5">
          <div class="col-12">
            <h1 class="text-center">Myers-Briggs Personlighetstest</h1>
          </div>
        </section>
        <section class="d-flex justify-content-center mt-5">
            <div class="col-10 col-md-6">
                <p>Du kommer nu svara på 24 frågor genom att skjuta en mätare höger eller vänster.
                Därefter presenteras du resultatet av ditt test.</p>
                <div class="text-center">
                    <button type="button" class="btnStart btn mt-3">Start test</button>
                </div>
            </div>
        </section>`);
    }

    renderQuestion(val) {
        let percent = Math.ceil((val / (this.questions.length - 1)) * 100);

        //"remember the last choice" when going back
        let sliderValue = 6;
        if (typeof app.user.answers[val] != 'undefined') {
            sliderValue = this.user.answers[val].val;
        }

        let html = `
                    <section class="row">
                        <div class="col-12 titleText">
                            <h1 class="text-center">Myers-Briggs Personlighetstest</h1>
                        </div>
                    </section>
                    <div class="d-flex align-items-center justify-content-center col-12 mt-1 mt-md-3">
                        <div class="d-flex align-items-center justify-content-center align-self-center sectionQuestions">
                            <p class="m-3 m-md-5 text-center">${this.questions[`${val}`].q}</p>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center">
                    <div class="d-flex align-items-center justify-content-center mt-1 mt-md-3 sectionAnswers">
                        <div class="d-flex justify-content-center align-items-center align-self-stretch col-6 col-sm-4 pb-2 pt-2 answer1">
                            <p class="mb-0 pl-3 text-center">${this.questions[`${val}`].a[0]}</p>
                        </div>
                        <div class="align-self-stretch d-inline-block d-none d-sm-block col-sm-4 middlePart">
                        </div>
                        <div class="d-flex justify-content-center align-items-center align-self-stretch col-6 col-sm-4 pb-2 pt-2 answer2">
                            <p class="mb-0 pr-3 text-center">${this.questions[`${val}`].a[1]}</p>
                        </div>
                    </div>
                    </div>
                    <section class="d-flex justify-content-center mt-2 fixed-sm-bottom2">
                    <div class="col-10 col-md-8 col-lg-8 mt-0 mb-3 p-0">
                        <div class="d-flex justify-content-between rangeNumbers">
                            <p class="text-center mb-0">0</p>
                            <p class="text-center mb-0">1</p>
                            <p class="text-center mb-0">2</p>
                            <p class="text-center mb-0">3</p>
                            <p class="text-center mb-0">4</p>
                            <p class="text-center mb-0">5</p>
                            <p class="text-center mb-0">6</p>
                            <p class="text-center mb-0">7</p>
                            <p class="text-center mb-0">8</p>
                            <p class="text-center mb-0">9</p>
                            <p class="text-center mb-0">10</p>
                            <p class="text-center mb-0">11</p>
                        </div>
                        <input id="slider" class="slider" type="range" min="0" value="${sliderValue}" max="11">
                    </div>
                    </section>
                    <div class="fixed-sm-bottom mb-2 ml-2 mr-2">
                    <section class="d-flex justify-content-center">
                    <div class="col-10 col-md-8 col-lg-6">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: ${percent}%;" aria-valuenow="${percent}" aria-valuemin="0" aria-valuemax="100">
                                ${percent}%
                            </div>
                        </div>
                    </div>
                    </section>`;
        if (val > 0) {
            html += `<section class="d-flex justify-content-between mt-2">
                        <div class="d-flex col-1 pl-0">
                            <button id="${val-1}" type="button" class="btnPrevious btn"><</button>
                        </div>`;
        } else {
            html += `<section class="d-flex justify-content-end mt-2">`;
        }
        html += `       <div class="d-flex justify-content-end col-1 pr-0">
                            <button id="${val+1}" type="button" class="btnNext btn">></button>
                        </div>
                    </section>
                </div>`;
        super.render(html);
    }

    renderScore() {
        let html = `
        <section class="row">
        <div class="col-12">
            <h2 class="text-center">Myers-Briggs Personlighetstest</h2>
        </div>
    </section>
        <section class="d-flex justify-content-center mt-3">

        <div class="row">
        `;
        for (let i = 0; i < Object.keys(this.user.score[0]).length; i++) {
            html += `
            <div class="col-6 col-sm-3 text-center">
                <p class="mb-2">${Object.keys(this.user.score[0])[i].split(/(?=[A-Z])/)[0].toLowerCase()}</p>
                    <svg width="50" height="200">
                        <rect class="rectfg" width="50" height="${(( (Object.values(this.user.score[0])[i]) / (66))  * 200)}" />
                    </svg>
                <p class="m-0 mb-3 text-center">${Object.keys(this.user.score[0])[i].split(/(?=[A-Z])/)[1].toLowerCase()}</p>
            </div>
          `;
        }
        html += `</div>
        </section>`;
        super.render(html);
    }
}