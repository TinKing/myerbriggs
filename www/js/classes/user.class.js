class User {
    constructor(app) {
        this.answers = [];
        this.score = [{
            introvertExtrovert: 0,
            intuitionSensing: 0,
            perceptionJudgement: 0,
            thinkingFeeling: 0
        }];
        //fo show
        this.results = [{
            introvertExtrovert: "",
            intuitionSensing: "",
            perceptionJudgement: "",
            thinkingFeeling: ""
        }];
        this.app = app;
    }

    answer(index, val) {
        this.answers[index] = {
            val: val,
            id: (parseInt(index) + 1)
        };
        if ((this.answers).length == 24)
            this.app.evaluateScore();
    }

    //save? :)
    saveAnswers() {
        JSON._save('answers', {
            answers: this.answers,
            score: this.score,
            result: this.results
        });
    }
}