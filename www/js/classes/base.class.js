class Base {
    constructor() {
        this.render();
    }

    render(html) {
        $("main").html(html);
    }

    registerEventHandlers(handlers) {
        $.each(handlers, function (key, fn) {
            $(document).on("click", "." + key, fn);
        });
    }
}